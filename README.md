# Desafio Python/Django FarmaJá
Desenvolva uma aplicação para gerenciar os pedido feitos a um Café.

Nesse Café os produtos são definidos pelo gerente, os clientes podem pedir qualquer bebida e customizar seu pedido.

Produtos:
- Expresso
- Chá: leite é opcional
- Cappuccino
- Chocolate quente
- Cookie: apenas sabor, chocolate ou baunilha;

Opções:
- Entrega ou retirada no local.
- Leite: integral ou desnatado 
- Tipo: simples, duplo ou triplo 
- Tamanho: pequeno, médio ou grande 
 
Todo pedido possui uma lista dos produtos com prados, status do pedido e preço total. 
Por simplicidade considere preço único para c ada produto, não importando as opções selecionadas.

# Importante
- Seguir a convenção PEP8
- Python 3.6
- Django 1.11 ou superior
- README com as instruções para rodar o projeto
- Arquivo requirements.txt na raiz do projeto com todas as dependências

# Requisitos

### Web
- Catálogo simples de produtos;
- Realizar um pedido (dica: uma solução é usando a api abaixo);

### API Rest
- Listar produtos;
- Criar um pedido;
- Ver detalhes de um pedido;
- Atualizar o status de um pedido;
- Cancelar um pedido;

Bônus
--
- **Documente o código**;
- Opções de filtragem de produtos e pedidos, no Django admin e/ou API;
- Implemente testes;
- Rodar usando docker.

Para entregar o desafio, faça um Fork deste repositório e crie um Merge Request.

Não existe um tempo determinado pra executar, mas ele conta. 
Levaremos em consideração: tempo x requisitos executados x qualidade do código.

Bom desenvolvimento!